function divElementEnostavniTekst(sporocilo) {
  //MISLIL SEM DA BI MORAL SPREMENITI TUKAJ.. BOY WAS I RIGHT.
  var spor = sporocilo;
 if(sporocilo.indexOf(';)') != -1){
   spor = (spor.replace(/;\)(?=[^<>]*(<|$))/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>'));
  } 
 if(sporocilo.indexOf(':)') != -1){
    spor = (spor.replace(/:\)(?=[^<>]*(<|$))/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>'));
  }  
 if(sporocilo.indexOf(':(') != -1){
    spor =(spor.replace(/:\((?=[^<>]*(<|$))/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>'));
  }  
 if(sporocilo.indexOf(':*') != -1){
    spor = (spor.replace(/:\*(?=[^<>]*(<|$))/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>'));
  }  
 if(sporocilo.indexOf('(y)') != -1){
    spor =  (spor.replace(/\(y\)(?=[^<>]*(<|$))/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>'));
  }    
 if(spor != sporocilo)  {
 return $('<div style="font-weight: bold"></div>').html(spor);}
 else{ return  $('<div style="font-weight: bold"></div>').text(sporocilo);}
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  //USERS DODANI
  socket.on('users', function(rezultat){
    $('#seznam-uporabnikov-na-kanalu').empty();
    for(var i = 0; i < rezultat.users.length; i++){
      $('#seznam-uporabnikov-na-kanalu').append(divElementEnostavniTekst(rezultat.users[i]));
    }
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  socket.on('ime', function(rzlt){
    $('#kanal').text(rzlt.kanal);
    $('#sporocila').append(divElementHtmlTekst(''));
  });
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  setInterval(function() {
      socket.emit('users');
  }, 1000);  


  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});